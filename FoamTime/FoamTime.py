import os
import glob
import subprocess
import zipfile
import shutil
import sys

class FoamTime(object):
    '''class FoamTime: a simple class to do some simple tasks on an OpenFOAM time'''
    def __init__(self,time,zipPrefix='',excludeZipList=[],excludeUnzipList=[]):
        self.workdir = os.getcwd()    
        self.time = time
        self.zipPrefix = zipPrefix
        self.excludeZipList = excludeZipList
        self.excludeUnzipList = excludeUnzipList
        self.processorList = glob.glob('processor*')
        self.zipName = self.zipPrefix+str(self.time)+'.zip'
        self.getStatus()


    def getStatus(self):
        # time is reconstructed
        if os.path.isdir(self.workdir+'/'+str(self.time)):
            self.reconstructed = True
        else:
            self.reconstructed = False
        # time is decomposed
        if len(self.processorList)>0:
            if os.path.isdir(self.workdir+'/'+self.processorList[0]+'/'+str(self.time)):
                self.decomposed = True
            else:
                self.decomposed = False
        else:
            self.decomposed = False
        # time is zipped
        if os.path.isfile(self.workdir+'/'+self.zipPrefix+str(self.time)+'.zip'):
            self.zipped = True
        else:
            self.zipped = False
        
    
    def printInfoTime(self,printInfo,outInfo='None',out='None',errInfo='None',err='None'):
        print 'Time '+str(self.time)+': '+printInfo+''
        if outInfo!='None': print out
        if errInfo!='None': print err
        
    
    def reconstructTime(self,region='region0',OFout=False):
            if self.decomposed==True:
                self.printInfoTime(printInfo='start reconstruction for region '+str(region)+'.')
                proc = subprocess.Popen('reconstructPar -region '+str(region)+' -time '+str(self.time)+'', stdout=subprocess.PIPE, shell=True)
                for line in iter(proc.stdout.readline, ''):
                    sys.stdout.write(line)
                (out, err) = proc.communicate()
                #if OFout==True: print out
                if proc.returncode==0:   #0 for success
                    self.reconstructed=True
                    self.printInfoTime(printInfo='time sucessfully reconstructed')
                    return True
                else:
                    self.printInfoTime(printInfo='reconstructTime error: reconstructPar failed')
                    return False
            else:
                self.printInfoTime(printInfo='reconstructTime error. decomposed time does not exist')
                return False
            
   
    def decomposeFieldsTime(self,OFout=False):
        if self.decomposed==False:
            if self.reconstructed==True:
                proc = subprocess.Popen('decomposePar -fields -time '+str(self.time)+'', stdout=subprocess.PIPE, shell=True)
                for line in iter(proc.stdout.readline, ''):
                    sys.stdout.write(line)
                (out, err) = proc.communicate()
                #if OFout==True: print out
                if proc.returncode==0:   #0 for success
                    self.decomposed=True
                    self.printInfoTime(printInfo='time sucessfully decomposed')
                    return True
                else:
                    self.printInfoTime(printInfo='decomposeTime error. Failed')
                    return False
            else:
                self.printInfoTime(printInfo='decomposeTime error. Reconstruced time does not exist.')
                return False
        else:
            self.printInfoTime(printInfo='decomposeTime info. already decomposed')
            return True


    def zipTime(self):
        '''for backward compatibility'''
        return self.zipTimeSmart()
    
    
    def zipTimeSmart(self):
        if self.zipped==False:
            return self.zipTimeForce()
        else:
            self.printInfoTime(printInfo='zipTime info. Already zipped.')
            return True
    
    
    def zipTimeForce(self):
        if self.reconstructed==True:
            try:
                self.printInfoTime(printInfo=' start zipping')
                myzipfile = ''+self.zipPrefix+str(self.time)+'.zip.tmp'
                myzipfilefinal = ''+self.zipPrefix+str(self.time)+'.zip'
                source = str(self.time)
                myzip = zipfile.ZipFile(myzipfile, 'w', zipfile.ZIP_DEFLATED)
                rootlen = len(source) + 1
                for base, dirs, files in os.walk(source):
                    for file in files:
                        included = True
                        for excludefile in self.excludeZipList:
                            if file==excludefile:
                                included = False
                                break
                            else:
                                included = True
                        if included==True:
                            print ' *zipping '+file+''
                            fn = os.path.join(base, file)
                            myzip.write(fn)
                        else:
                            pass
                myzip.close()
                os.rename(myzipfile,myzipfilefinal)
                self.zipped=True
                self.printInfoTime(printInfo='time sucessfully zipped')
                return True
            except:
                self.printInfoTime(printInfo='zipTime error')
                return False
        else:
            self.printInfoTime(printInfo='zipTime error. Need reconstructed time.')
            return False
    
    
    def unzipTimeSmart(self):
        if self.reconstructed==False:
            return self.unzipTimeForce()
        else:
            self.printInfoTime(printInfo='time already unzipped')
            return True
            
    
    def unzipTimeForce(self):
        try:
            shutil.rmtree(self.time)
        except:
            pass
        if self.zipped==True:
            try:
                self.printInfoTime(printInfo=' start unzipping')
                myzipfile = ''+self.zipPrefix+str(self.time)+'.zip'
                with zipfile.ZipFile(myzipfile, 'r') as myzip:
                    for member in myzip.namelist():
                        dounzip = True
                        for excludefile in self.excludeUnzipList:
                            if os.path.basename(member)==excludefile:
                                dounzip = False
                                break
                            else:
                                dounzip = True
                        if dounzip==True:
                            myzip.extract(member)
                            print ' *unzip '+member+''
                return True
            except:
                self.printInfoTime(printInfo='unzip error')
                return False
        else:
            self.printInfoTime(printInfo='unzip error. Zipped time does not exist.')
            return False


    def rmZippedTime(self):
        self.getStatus()
        if self.zipped==True:
            try:
                pathToRemove = ''+self.workdir+'/'+self.zipPrefix+str(self.time)+'.zip'
                os.remove(pathToRemove)
                self.zipped = False
                self.printInfoTime(printInfo='zipped time removed')
                return True
            except:
                self.printInfoTime(printInfo='error. Cannot remove zipped time')
                return False
        else:
            self.printInfoTime(printInfo='zipped time does not exist. Cannot remove.')
            return False
    
    
    def rmReconstructedTime(self):
        self.getStatus()
        if self.reconstructed==True:
            try:
                pathToRemove = ''+self.workdir+'/'+str(self.time)+''
                shutil.rmtree(pathToRemove)
                self.reconstructed = False
                #self.printInfoTime(printInfo='reconstructed time removed')
                return True
            except Exception, err:
                self.printInfoTime(printInfo='error. Cannot remove reconstructed time')
                return False
        else:
            self.printInfoTime(printInfo='Reconstructed time does not exist. Cannot remove.')
            return False
    
    
    def rmDecomposedTime(self):
        self.getStatus()
        if self.decomposed==True:
            for processor in self.processorList:
                try:
                    pathToRemove = ''+self.workdir+'/'+processor+'/'+str(self.time)+''
                    shutil.rmtree(pathToRemove)
                except Exception, err:
                    self.printInfoTime(printInfo='error. Cannot remove decomposed time')
                    return False
            self.decomposed = False
            self.printInfoTime(printInfo='decomposed time removed')
            return True
        else:
            self.printInfoTime(printInfo='decomposed time does not exit. Cannot remove.')  
            return False
       
    
    def rsyncZippedTime(self,syncRootDst='',syncRootSrc=''):
        self.getStatus()
        dstPath = str()
        srcPath = str()
        if syncRootDst=='':
            dstPath = self.zipName
        else:
            dstPath = syncRootDst+'/'+self.zipName
        if syncRootSrc=='':
            srcPath = self.zipName
        else:
            srcPath = syncRootSrc+'/'+self.zipName
        if self.zipped==True:
            self.printInfoTime(printInfo='start rsync:')
            proc = subprocess.Popen(['rsync -zvr -e ssh '+srcPath+' '+dstPath+''], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print out
            return True
        else:
            self.printInfoTime(printInfo='zipped time does not exist. Cannot sync to syncRootDest')
            return False
       
    def simpleSyncTime(self,syncRootDst='',syncRootSrc=''):
        '''simple transfer without checking neither srcfile exists nor dstfile already exist.'''
        dstPath = str()
        srcPath = str()
        if syncRootDst=='':
            dstPath = self.zipName
        else:
            dstPath = syncRootDst+'/'+self.zipName
        if syncRootSrc=='':
            srcPath = self.zipName
        else:
            srcPath = syncRootSrc+'/'+self.zipName
        try:
            self.printInfoTime(printInfo='start rsync')
            proc = subprocess.Popen(['rsync -zvr -e ssh '+srcPath+' '+dstPath+''], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print out
            return True
        except Exception, err:
            self.printInfoTime(printInfo='error. Cannot simpleSyncTime')
            return False
            
    
    def rsyncReconstructedTime(self,src='',dst=''):
        self.getStatus()
        if dst=='':
            dstPath = '.'
        else:
            dstPath = dst
        if src=='':
            srcPath = self.time
        else:
            srcPath = src+'/'+self.time
        try:
            self.printInfoTime(printInfo='start rsync')
            proc = subprocess.Popen(['rsync -zvr -e ssh '+srcPath+' '+dstPath+''], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
            print(out)
            return True
        except Exception, err:
            self.printInfoTime(printInfo='error. Cannot rsyncReconstructedTime')
            return False
            
            
        
        
        
        
