import os
import subprocess
import shutil
import zipfile
import sys
import statvfs
import time
import re

sys.path.insert(0,'../FoamTime')
from FoamTime import FoamTime

class FoamTimes(object):
    """FoamTimes"""

    def __init__(self,zipPrefix='',localDir='',remoteDir='',server='',unzipLocal=True,removeRemoteZip=False,removeLocalZip=False,removeRemoteReTime=False):
        self.workDir = os.getcwd()
        self.zipPrefix = zipPrefix
        if localDir=='':
            self.localDir = self.workDir
        else:
            self.localDir = localDir
        self.remoteDir = remoteDir
        self.server = server
        self.unzipLocal = unzipLocal
        self.removeRemoteZip = removeRemoteZip
        self.removeLocalZip = removeLocalZip
        self.removeRemoteReTime = removeRemoteReTime
        
    def getLocalZipList(self):
        proc = subprocess.Popen(['ls '+self.localDir], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        if proc.returncode == 0:
            pass
            #print "geting file list successful"
        fileList = str.splitlines(out)
        zipList = []
        for i in range(len(fileList)):
            iszip = fileList[i]
            #if iszip[-4:]=='.zip':
            if iszip[-4:]=='.zip' and self.is_number(iszip[:-4])==True:
                zipList.append(iszip)
        #print "Nr of zipfiles found: " + str(len(zipList))
        return zipList
    
    
    def getRemoteZipList(self):
        proc = subprocess.Popen(['ssh '+self.server+' ls '+self.remoteDir], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        if proc.returncode == 0:
            print "geting file list successful"
        fileList = str.splitlines(out)
        zipList = []
        for i in range(len(fileList)):
            iszip = fileList[i]
            #if iszip[-4:]=='.zip':
            if iszip[-4:]=='.zip' and self.is_number(iszip[:-4])==True:
                zipList.append(iszip)
        #print "Nr of zipfiles found: " + str(len(zipList))
        return zipList
    
    
    def getLocalReTimeList(self):
        dirlist = os.listdir(self.workDir)
        retimelist = list() #for "REconstructed TIME LIST"
        for dire in dirlist:
            if self.is_number(dire):
                retimelist.append(dire)
            else:
                pass
        return retimelist


    def getRemoteReTimeList(self):
        proc = subprocess.Popen(['ssh '+self.server+' find '+self.remoteDir+' -maxdepth 1 -type d'], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        out = out.split()
        retimelist = list() #for "REconstructed TIME LIST"  
        for dire in out:
            dironly = dire[len(self.remoteDir)+1:]
            if self.is_number(dironly):
                retimelist.append(dironly)
            else:
                pass
        return retimelist

    
    def is_number(self,s):
        try:
            float(s)
            return True
        except ValueError:
            return False
                
                
    def unzipLocalZip(self,force=False):
        localZipList = self.getLocalZipList()
        if len(localZipList) > 0:
            for zipTime in localZipList:
                time = zipTime[:-4]
                oftime = FoamTime(time)
                if force==True:
                    oftime.unzipTimeForce()
                else:
                    oftime.unzipTimeSmart()
        else:
            print 'No zippedTime found.'
            
                    
    def getRemoteZippedTime(self):
        def rmRemoteZippedTime(oftime):
            oftime.printInfoTime(printInfo='Remove zip on server.')
            proc = subprocess.Popen(['ssh '+self.server+' rm '+self.remoteDir+'/'+oftime.zipName], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()
        
        remoteZipList = self.getRemoteZipList()
        if len(remoteZipList) > 0:
            for zippedTime in remoteZipList:
                time = zippedTime[:-4]
                oftime = FoamTime(time)
                syncOut = False
                if oftime.reconstructed==False:
                    syncRootSrc = self.server+':'+self.remoteDir
                    syncOut = oftime.simpleSyncTime(syncRootDst=self.localDir,syncRootSrc=syncRootSrc)
                    oftime.getStatus()
                else:
                    oftime.printInfoTime(printInfo='Already unzipped on local. Transfer abored.')
                    rmRemoteZippedTime(oftime=oftime)
                if self.unzipLocal==True and syncOut==True:
                    oftime.unzipTimeSmart()
                if self.removeRemoteZip==True and syncOut==True:
                    rmRemoteZippedTime(oftime=oftime)
#                    oftime.printInfoTime(printInfo='Remove zip on server.')
#                    proc = subprocess.Popen(['ssh '+self.server+' rm '+self.remoteDir+'/'+oftime.zipName], stdout=subprocess.PIPE, shell=True)
#                    (out, err) = proc.communicate()
                if self.removeLocalZip==True and syncOut==True:
                    if os.path.isfile(self.localDir+'/'+oftime.zipName)==True:
                        oftime.printInfoTime(printInfo='Remove zip on local.')
                        os.remove(self.localDir+'/'+oftime.zipName)
        else:
            print 'No zipped time found on server.'
    
    
    def getRemoteZippedTimeLoop(self, sleepTime=10):
        while True:
            remoteZipList = self.getRemoteZipList()
            if len(remoteZipList) < 1:
                print "No zipped time found on server"
                print 'Sleep for '+str(sleepTime)+' esc'
                time.sleep(sleepTime)
                #return
            else:
                self.getRemoteZippedTime()
                #return def
    
    
    def getRemoteReTime(self):
        def rmRemoteReTime(oftime):
            oftime.printInfoTime(printInfo='Remove reconstructed time on server.')
            proc = subprocess.Popen(['ssh '+self.server+' rm '+self.remoteDir+'/'+oftime.time], stdout=subprocess.PIPE, shell=True)
            (out, err) = proc.communicate()

        remoteReTimeList = self.getRemoteReTimeList()
        rsyncOut = False
        for time in remoteReTimeList:
            oftime = FoamTime(time)
            if oftime.reconstructed==False:
                src = self.server+':'+self.remoteDir
                dst = self.localDir
                rsyncOut = oftime.rsyncReconstructedTime(src=src,dst=dst)
                if self.removeRemoteReTime==True and syncOut==True:
                    rmRemoteReTime(oftime)
            else:
                oftime.printInfoTime(printInfo='Reconstructed time already exist.')
                
                    
        
        