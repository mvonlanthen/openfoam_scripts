#!/usr/bin/env python

#======================================
# name:    LogAnalyzer3.py
# author:  Marc Immer
#          Marcel Vonlanthen
# date:    once upon a time in the lab...
# contact: vonlanthen@arch.ethz.ch
# purpose: Analyze OpenFOAM solver log
# version: 3.0
#
# test run in spyter: runfile('/nas/marcel/OpenFOAM/marcel-2.2.1/run/nesting_15c3r/LogAnalyzer3.py', wdir=r'/nas/marcel/OpenFOAM/marcel-2.2.1/run/nesting_15c3r', args='-log 15c3r/output/solver.out')
#======================================

#======================================
# import modules
#======================================
import re
import os
import sys
import argparse
import shutil

import numpy as np
#import datetime

import matplotlib.pyplot as plt

#======================================
# parse arguments
#======================================
parser = argparse.ArgumentParser(description='Purpose: change the world, dude! And also analyze OpenFOAM solver log.')
parser.add_argument('-log',
                    dest='logs',
                    type=str,
                    nargs='+',
                    required=False,
                    default='output/solver.out',
                    help='path to solver log file (/my/path/whatever/solver.log). Default: system/solver.out. Multiple logs can be specified.'        
                    )
parser.add_argument('-localcopy',
                    dest="localCopy",
                    action="store_true",
                    default=False,
                    help='copy locally LogAnalyzer3.py. usful for creating custom plots.')
parser.add_argument('-plot',
                    dest="plot",
                    action="store_true",
                    default=False,
                    help='generate plots and show them. It only works locally with matplotlib (which is a MUST have...).')

args = parser.parse_args()

#======================================
# function definitions
#======================================

def round2(nr,d):
    return np.round(nr*pow(10,d))/pow(10,d)

def getTimes(filepath):
    crs = open(filepath, 'r')
    runtime=[]
    timestep=[]
    for line in crs:
        # This regex finds all numbers in a given string.
        # It can find floats and integers writen in normal mode (10000) or with power of 10 (10e3).
        #match = re.findall('ClockTime = \d+', line)
        if line[0:5]==r'Execu':
            match = re.findall('[-+]?\d*\.?\d+e*[-+]?\d*', line)
            if len(match)>0:
                runtime.append(match)
        if line[0:6]==r'Time =':
            match = re.findall('[-+]?\d*\.?\d+e*[-+]?\d*', line)
            if len(match)>0:
                timestep.append(match)
    executionTime=[float(row[0]) for row in runtime]
    clockTime=[float(row[1]) for row in runtime]
    timestep=[float(a[0]) for a in timestep]
    return np.array(executionTime), np.array(clockTime), np.array(timestep)

def getHeader(filepath):
    crs = open(filepath, 'r')
    nProcs = int()
    case = str()
    counter = 0
    for line in crs:
        if line[0:8]==r'Case   :':
            case = line[8:-1]
            counter = counter+1
        if line[0:8]==r'nProcs :':
            match = re.findall('[-+]?\d*\.?\d+e*[-+]?\d*', line)
            if len(match)>0:
                nProcs = match[0]
                counter = counter+1
        if counter==2:
            break
    headerDict = {'case':case,'nProcs':nProcs}              
    return headerDict

def calcLogDict(log):
    execTime,clockTime,timestep=getTimes(log)
    entries=len(timestep)
    
    timestep=timestep[0:entries]
    
    execTime = execTime[0:entries]
    clockTime=clockTime[0:entries]
        
    execRunTime = execTime-execTime[0]
    clockRunTime=clockTime-clockTime[0]
    
    execRunDiff = np.diff(execRunTime)
    clockRunDiff = np.diff(clockRunTime)
    
    logDict = {
               'ts':timestep,
               'ert':execRunTime,
               'crt':clockRunTime,
               'erd':execRunDiff,
               'crd':clockRunDiff
               }
    
    return logDict
    
def printLog(logDict,logName,header):
    timestep = logDict['ts']
    clockRunTime = logDict['crt']
    
    print(logName)
    print('===============')
    print('case =   '+header['case'])
    print('nProcs = '+str(header['nProcs']))
    print('avg dt = '+str(round2(np.mean(np.diff(clockRunTime)),4)) )      #ave dt: number of seconds per time step (average)
    print('s/dt =   '+str( round2(clockRunTime[-1]/(len(clockRunTime)-1),4) ) )   #s/ts: seconds per timestep (total time over the number of timestep)
    print('dt/s =   '+str(round2(len(timestep)/clockRunTime[-1],4)))   # ts/s: timestep per second (total nb of timestep over total clock time)
    print('dt[-1] = '+str(clockRunTime[-1]-clockRunTime[-2])) #dt: last timestep duration (in second)
    
    s_ssim=clockRunTime[-1]/(timestep[-1]-timestep[0])
    print('s/ssim =   '+str(round2(s_ssim,4)))              #s/ssim: number of seconds to achive one second of simulation
    print('min/ssim = '+str(round2(clockRunTime[-1]/60.0/(timestep[-1]-timestep[0]),4))) #min/ssim: number of minuts to achive one second of simulation
    print('h/ssim =   '+str(round2(clockRunTime[-1]/3600.0/(timestep[-1]-timestep[0]),4))) #h/ssim: number ofhours to achive one second of simulation
    
    print('first ts = '+str(timestep[0]))
    print('last ts =  '+str(timestep[-1]))
    print('sum dt =   '+str(round2((clockRunTime[-1]-clockRunTime[0])/3600.0,4)))
    print('')

def printLog2(logDict,logName,header):
    timestep = logDict['ts']
    clockRunTime = logDict['crt']
    
    print(logName)
    print('===============')
    print('case =   '+header['case'])
    print('nProcs = '+str(header['nProcs']))
    
    datas = []
    datas.append(np.mean(np.diff(clockRunTime)))             #ave dt: number of seconds per time step (average)
    datas.append(clockRunTime[-1]/(len(clockRunTime)-1))     #s/ts: seconds per timestep (total time over the number of timestep)
    datas.append(len(timestep)/clockRunTime[-1])             #ts/s: timestep per second (total nb of timestep over total clock time)
    datas.append(clockRunTime[-1]-clockRunTime[-2])          #dt: last timestep duration (in second)
    datas.append(clockRunTime[-1]/(timestep[-1]-timestep[0]))             #s/ssim: number of seconds to achive one second of simulation
    datas.append(clockRunTime[-1]/60.0/(timestep[-1]-timestep[0]))        #min/ssim: number of minuts to achive one second of simulation
    datas.append(clockRunTime[-1]/3600.0/(timestep[-1]-timestep[0]))      #h/ssim: number ofhours to achive one second of simulation
    datas.append(timestep[0])                   #first ts
    datas.append(timestep[-1])                  #last ts
    datas.append((clockRunTime[-1]-clockRunTime[0])/3600.0)       #total wall clock run time
    
    headers = []
    headers.append('avg dt')
    headers.append('s/dt')
    headers.append('dt/s')
    headers.append('dt[-1]')
    headers.append('s/ssim')
    headers.append('min/ssim')
    headers.append('h/ssim')
    headers.append('ts[0]')
    headers.append('ts[-1]')
    headers.append('sum dt')
    
    row_dataFormat = str()
    row_headerFormat = str()
    for data in datas:
        # if the data is too long, give more space in the table
        if data>10000:      
            row_dataFormat = row_dataFormat+"{:>15.3f}"
            row_headerFormat = row_headerFormat+"{:>15}"
        # otherwise, use the standard 10 digit space
        else:
            row_dataFormat = row_dataFormat+"{:>10.3f}"
            row_headerFormat = row_headerFormat+"{:>10}"
     
    #row_headerFormat ="{:>12}" * (len(headers))
    print(row_headerFormat.format(*headers))
    #row_dataFormat ="{:>12.3f}" * (len(headers))
    print(row_dataFormat.format(*datas))
    
    print('')
    
    
def cutDataForPlot(arrayA,arrayB):
    if len(arrayA)>len(arrayB):
        cutEndIndex = len(arrayA)-len(arrayB)
        arrayACut = arrayA[0:-cutEndIndex]
        arrayBCut = arrayB
    elif len(arrayA)<len(arrayB):
        cutEndIndex = len(arrayB)-len(arrayA)
        arrayACut = arrayA
        arrayBCut = arrayB[0:-cutEndIndex]
    else:
        arrayACut = arrayA
        arrayBCut = arrayB
    return arrayACut, arrayBCut
    

            
#======================================
# main
#======================================
runAbsPath = os.getcwd()


if args.localCopy==True:
    shutil.copy(os.path.abspath(os.path.realpath(__file__)) , runAbsPath)
    
# get logs path, absolut path and basename
logsPath = [] 
if type(args.logs)==str:
    logsPath.append(args.logs)
else:
    logsPath = args.logs

logsAbsPath = [os.path.abspath(log) for log in logsPath]
    
# check if log lifes exist
for log in logsAbsPath:
    if os.path.exists(log):
        pass
    else:
        sys.exit('Solver log "'+log+'" does not exist. Check your path. Exit.')
  
logsBname = [os.path.basename(log) for log in logsPath]

# get log data
logDicts = []
headerDicts = []
for log in logsAbsPath:
    logDicts.append(calcLogDict(log))
    headerDicts.append(getHeader(log))
    
# print data for each log
for i in range(len(logDicts)):
    printLog2(logDicts[i],logsBname[i],headerDicts[i])
    

if args.plot==True:
    for i in range(len(logDicts)):
        timestep = logDicts[i]['ts']
        clockRunTime = logDicts[i]['crt']
        logDict = logDicts[i]
        logBname = logsBname[i]
        case = headerDicts[i]['case']
        nProcs = headerDicts[i]['nProcs']
        # cut data at the right lenght
        tsCut,crdCut = cutDataForPlot(logDict['ts'],logDict['crd'])
        #gen plot    
        fig = plt.figure(figsize=(10,5))
        fig.text(0.15, 0.88, 'log name: '+logBname+'\n'+'case:  '+case+'\n'+'nProcs: '+str(nProcs),fontsize=8,verticalalignment='top',bbox=dict(ec='black', fc='white', alpha=1.0))    
        plt.plot(tsCut,crdCut)
        plt.grid(True)
        plt.ylim( (0.5*clockRunTime[-1]/(len(clockRunTime)-1),2*clockRunTime[-1]/(len(clockRunTime)-1)) )
        plt.title('Timestep duration')    
        plt.xlabel('simulation time [s]')
        plt.ylabel('clockRunDiff')        
        plt.savefig('logPlot_'+str(i)+'_'+str(logBname)+'.pdf')
    #plt.show()


print('done')
