#!/usr/bin/env python3

#======================================
# name:    foamLineToPoints.py
# author:  Marcel Vonlanthen
# date:    18.07.2013
# contact: marcelv@student.ethz.ch
# purpose: Create a list of point which discretize the line defined by ptStart and ptEnd.
#          points are separated by a space and embedded in brackets      
# version: 1.0
#======================================

#======================================
# import modules
#======================================
#import os
import sys
#import shutil
import argparse

import numpy as np


#======================================
# parse arguments
#======================================
parser = argparse.ArgumentParser(description='purpose: Create a list of point which discretize the line defined by ptStart and ptEnd. Points are separated by a space and embedded in brackets. This is who OpenFOAM reads a list of 3D points.')
parser.add_argument('-ptstart',
                    dest='ptStart',
                    type=float,
                    required=True,
                    nargs='+',
                    help='Start point of the line'        
                    )
parser.add_argument('-ptend',
                    dest='ptEnd',
                    type=float,
                    required=True,
                    nargs='+',
                    help='End point of the line'        
                    )
parser.add_argument('-equidist',
                    dest='equiDist',
                    type=float,
                    required=False,
                    default=None,
                    help='Equidistant spacing between points. Usage: "-equidist n" with n the number of points (start and end included in n)'        
                    )
parser.add_argument('-expdist',
                    dest='expDist',
                    type=float,
                    required=False,
                    nargs='+',
                    default=None,
                    help='exponantial spacing between points. Usage: "-expdist a b" with a the initial spacing and b the growth. example: 10% growth with initial spacing of 3: -expdist 3 0.1'        
                    )
parser.add_argument('-outfile',
                    dest='outFile',
                    type=str,
                    required=True,
                    help='Output file. Name only or path'        
                    )
parser.add_argument('-v',
                    #'--verbrose',
                    dest='verbrose',
                    type=int,
                    choices=[0, 1, 2],
                    default=0,
                    help='verbrosity level. Default = 0.'
                    )
parser.add_argument('-plt',
                    dest='plot',
                    type=bool,
                    default=False,
                    help='Plot of point distribution (need python library matplotlib and matplotlib TK backend.)'
                    )
args = parser.parse_args()

#======================================
# main
#======================================
def equiDist(ptStart,equiDistData,lineVec,lineLength):
    '''
    '''
    pts = list()
    pts.append(ptStart)
    distVec = (1/(equiDistData-1))*lineVec
    while np.linalg.norm(pts[-1]-ptStart)<(1.001*lineLength):  #multiplied by 1.001 to trick floating point operation
        pts.append(pts[-1]+distVec)
    pts.pop(-1)
    return pts
        
def expDist(ptStart,expDistData,lineVec,lineLength):
    '''
    '''
    pts = list()
    #start point
    pts.append(ptStart)
    n = 0
    while np.linalg.norm(pts[-1]-ptStart)<(1.001*lineLength):  #multiplied by 1.001 to trick floating point operation
        dist = ((1+expDistData[1])**n)*(expDistData[0]/lineLength)
        distVec = np.zeros(3)
        distVec = dist*lineVec
        pts.append(pts[-1]+distVec)
        n = n+1
    pts.pop(-1)
    return pts


# some pre-work on the line
ptStart = np.array(args.ptStart)
ptEnd = np.array(args.ptEnd)
lineVec = ptEnd-ptStart
lineLength = np.linalg.norm(lineVec)

# big if loop between all dist methodes
pts = list()
if args.equiDist!=None:      
    pts = np.array(equiDist(ptStart,args.equiDist,lineVec,lineLength))
elif args.expDist!=None: 
    pts = np.array(expDist(ptStart,args.expDist,lineVec,lineLength))
    
# open parser fo output file
ostream = open(args.outFile,'w')
for pt in pts:
    ostream.write('( ')
    for i in range(len(pt)):
        ostream.write(str(pt[i])+' ')
    ostream.write(')\n')
ostream.close()

if args.verbrose>=1:
    print('******')
    print('report')
    print('******')
    print('nb points = '+str(len(pts)))

# plot distribution
if args.plot == True:
    import matplotlib.pyplot as plt
    ptsDist = np.zeros(pts.shape[0]-1)
    for i in range(len(pts)-1):
        ptsDist[i] = np.linalg.norm(pts[i+1]-pts[i])
    plt.figure()
    plt.plot(ptsDist,marker='o')
    plt.legend()
    plt.grid(True)
    plt.title('Point Distribution')
    plt.xlabel('point i')
    plt.ylabel('pt to pt distance')
    plt.show()





