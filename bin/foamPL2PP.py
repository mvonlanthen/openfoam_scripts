#!/usr/bin/env python3

#======================================
# name:    foamPL2PP.py
# author:  Marcel Vonlanthen
# date:    05.08.2013
# contact: marcelv@student.ethz.ch
# purpose: read  all openfoam post-processing PostLines (PL) and write it into a single file
#          similar to a PointProbes (PP)...  
# version: 1.0
#======================================

#======================================
# import modules
#======================================
import os
import sys
#import shutil
import argparse
import csv

#import numpy as np
#import scipy as sp


#======================================
# parse arguments
#======================================
parser = argparse.ArgumentParser(description='purpose: change the world, dude! (and some other minor tricks...)')
parser.add_argument('-srcdir',
                    dest='srcDir',
                    type=str,
                    required=True,
                    help='Source directory (a path)'        
                    )
parser.add_argument('-srcline',
                    dest='srcLine',
                    type=str,
                    required=True,
                    help='name of the line'        
                    )
parser.add_argument('-tgtfile',
                    dest='tgtFile',
                    type=str,
                    required=True,
                    help='target file. Add path to change location'        
                    )
        
parser.add_argument('-tsrange',
                    dest='tsRange',
                    type=float,
                    default=list(),  #empty list
                    nargs='+',
                    help='select time range. Will use all timestep awailable in this range, including the given limits (if the limit exists).'
                    )
parser.add_argument('-floatcheck',
                    dest='floatCheck',
                    type=bool,
                    default=True,
                    help='test if time dir in srcDir are really a timestep (means a float). Should speedup exectution (really?). Use it with caution!'
                    )
parser.add_argument('-rmline',
                    dest='rmLine',
                    action='store_true',
                    default=False,
                    help='remove line after copying it.'
                    )
#parser.add_argument('-append',
#                    dest='append',
#                    type=bool,
#                    default=False,
#                    help='append to existing file. NOT YET INCLUDED.'
#                    )

                    
args = parser.parse_args()

#======================================
# function definitions
#======================================
def isNumber(s):
    '''Return True if 's' is a float'''
    try:
        float(s)
        return True
    except ValueError:
        return False

#def readcsv(csvfile,delimiter):
##    data = list()
#    dataInRow = list( csv.reader(open(csvfile,delimiter=delimiter)))
#    return dataInRow

def getTimeList(tsMin=None,tsMax=None,floatCheck=True):
    timeList = os.listdir(args.srcDir)
    newTimeList = list()
    for i in range(len(timeList)):
        writeTime = False
        # check if time is a number, to discreminate wrong files
        if floatCheck==True:
            if isNumber(timeList[i]):
                writeTime = True                           
            else:
                writeTime = False
        else:
            pass
        # check if time is in tsRange.
        if tsMin!=None and tsMax!=None:
            if float(timeList[i])<tsMin or float(timeList[i])>tsMax:
                writeTime = False
            else:
                writeTime = True
        else:
            pass
        # copy value into newTimeList
        if writeTime==True:
            newTimeList.append(timeList[i]) 
        else:
            pass           
    timeList = newTimeList
    timeList.sort()    
#    print(timeList)
    return timeList

def writePPfile(timeList,rmLine=False):
    of = open(args.tgtFile, 'w')
    index = 0
    for time in timeList:
        print('timeStep ts = '+time)
        srclinepath = args.srcDir+'/'+time+'/'+args.srcLine
        lineData = [row for row in csv.reader(open(srclinepath), delimiter='\t')]
        # write line x, y, z and "time" in the target file
        if index==0:
            xyz = ['x','y','z']
            for i in range(3):
                of.write('#         '+xyz[i]+'         ')
                for j in range(len(lineData)):
                    of.write(lineData[j][i]+'         ')
                of.write('\n')
            of.write('#       Time\n')
        index = index+1
        # write the entier source file on one line in the target file
        of.write('       '+time+'       ')
        for i in range(len(lineData)):
            of.write('(')
            for j in range(3,len(lineData[0])):
                of.write(lineData[i][j]+' ')
            of.write(')     ')
        of.write('\n')
        # remove line if requested
#        print(rmline)
        if rmLine==True:
            os.remove(srclinepath)
            print('remove timeStep ts = '+time)
        else:
            pass
    of.close()
  
#======================================
# main
#======================================
runDir = os.getcwd()

# generate timeList
if len(args.tsRange)>0:
    if args.tsRange[0]<args.tsRange[1]:
        timeList = getTimeList(tsMin=args.tsRange[0],tsMax=args.tsRange[1],floatCheck=args.floatCheck)
    else:
        print('tsRange[0]>tsRange[1]. Exit.')    
        sys.exit()
else:
    timeList = getTimeList(floatCheck=args.floatCheck)

# check timeList lenght   
if len(timeList)>0:
    pass
else:
    print('timeList is empty. Exit.')    
    sys.exit()

print(args.rmLine)
writePPfile(timeList,rmLine=args.rmLine)