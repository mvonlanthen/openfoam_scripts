#!/usr/bin/env python

#======================================
# import modules
#======================================
import os
import sys
import argparse

#classpath = os.path.dirname(os.path.realpath(__file__))[:-4]
#print(classpath)

sys.path.insert(0,'/home/marcel/OpenFOAM/OpenFOAM_scripts/FoamTime')
from FoamTime import FoamTime
sys.path.insert(0,'/home/marcel/OpenFOAM/OpenFOAM_scripts/FoamTimes')
from FoamTimes import FoamTimes

parser = argparse.ArgumentParser(description='Get status of results in a local OpenFOAM case directory. The optional arguments helps to perform simple action')
parser.add_argument('-z',
                    '--zip-timestep',
                    action="store_true",
                    dest="dozip",
                    help='zip timesteps which are not already zipped'
                    )
parser.add_argument('-uz',
                    '--unzip-timestep',
                    action="store_true",
                    dest="dounzip",
                    help='unzip timesteps which are not already unzipped'
                    )
parser.add_argument('-dr',
                    '--delete-reconstructed',
                    action="store_true",
                    dest="del_rts",
                    help='delete reconstructed timestep')
parser.add_argument('-dz',
                    '--delete-zipped',
                    action="store_true",
                    dest="del_zts",
                    help='delete zipped timestep')
parser.add_argument('-tsr',
                    '--timesteprange',
                    type=float,
                    dest='tsRange',
                    nargs=2,
                    required=False,
                    help='range [min;max] of timestep on which to operate.'
                    )
parser.add_argument('-v',
                    '--verbrose',
                    type=int,
                    dest='verbrose',
                    choices=[0, 1, 2],
                    default=0,
                    help='verbrosity level. Default = 0.'
                    )
args = parser.parse_args()

#======================================
# main
#======================================
workdir = os.getcwd()

oftimes = FoamTimes()
rts = oftimes.getLocalReTimeList() #rts stands for 'Reconstructed TimeS'
zts = oftimes.getLocalZipList()
zts_noex = []
for zt in zts:
    zts_noex.append(zt[:-4])

# check for match and unmatch. use of 'set' which should be faster for big lists (I read it, I do not check it myself...)
a = set(zts_noex)
b = set(rts)
rzts_match =  list(a.intersection(b))
rts_unmatch_zts = list(b-a)
zts_unmatch_rts = list(a-b)

# sort list
rts.sort()
zts_noex.sort()
rts_unmatch_zts.sort()
zts_unmatch_rts.sort()

print('=======================')
print('OpenFOAM results status')
print('=======================')
print('Nb of reconstructed timestep: '+str(len(rts)))
print('Nb of zipped timestep:        '+str(len(zts_noex)))
print('Nb of timestep which are not zipped:   '+str(len(rts_unmatch_zts)))
print('Nb of timestep which are not unzipped: '+str(len(zts_unmatch_rts)))

# functions
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def getFoamTs(nb):
    '''
    converte a float or int (nb) into a valid OpenFOAM timestep (26 instead of 26.0) and return
    it as nbstr, a string. nbstr can be used in a path for example.
    '''
    nbstr = str(nb)
    if nbstr[-2:]=='.0':        # if nbstr='26.0'
        return nbstr[:-2]
    elif nbstr[-1]!='0':        #if nbstr='2.8' of '1.897' or '1.008': valid OF time
        return nbstr
    elif nbstr[-1]=='0' and len(nbstr)==1:   #if nbstr='0': valid OF time
        return nbstr
    else:                      #if nbrstr='26.000' or '3.4500'             
        trailingZero = True
        while trailingZero==True:
            nbstr = nbstr[:-1]
            if nbstr[-1]=='0':
                pass
            elif nbstr[-1]=='.':
                nbstr = nbstr[:-1]
                trailingZero = False
                return nbstr
            else:
                return nbstr

def truncateSortedList(sortedList,myMin,myMax):
    '''
    '''
    if len(sortedList)==0:
        return sortedList
    else:
        minIndex = None
        maxIndex = None
        # pop entries below myMin
        for i in range(len(sortedList)):
            if float(sortedList[i])>=myMin:
                minIndex = i
                break
            else:
                pass
        if minIndex!=None:
            sortedList = sortedList[minIndex:]
        else:
            sortedList = []
        # pop entries above my Max
        for i in range(len(sortedList)):
            if float(sortedList[i])>myMax:
                maxIndex = i
                break
            else:
                pass
        if maxIndex!=None:
            sortedList = sortedList[:maxIndex]
        else:
            pass
    # return truncated sorted list
    return sortedList

def dounzip(zts_unmatch_rts):
    if len(zts_unmatch_rts)!=0:
        for zt in zts_unmatch_rts:
            if is_number(zt):
                oftime = FoamTime(zt)
                oftime.unzipTimeSmart()
            else:
                pass
        print(str(len(zts_unmatch_rts))+' timesteps unzipped.')
    else:
        print(str(len(zts_unmatch_rts))+' timestep exists only as zip. No unzip action requiered.')

def dozip(rts_unmatch_zts):
    if len(rts_unmatch_zts)!=0:
        for rt in rts_unmatch_zts:
            if is_number(rt):
                oftime = FoamTime(rt)
                oftime.zipTimeSmart()
            else:
                pass
        print(str(len(rts_unmatch_zts))+' timesteps zipped.')
    else:
        print(str(len(rts_unmatch_zts))+' timestep exists only as reconstructed. No zip action requiered.')

def del_rts(rts):
    if len(rts)!=0:
        for rt in rts:
            if is_number(rt):
                oftime = FoamTime(rt)
                oftime.rmReconstructedTime()
            else:
                pass
        print(str(len(rts))+' reconstructed timesteps deleted.')
    else:
        print(str(len(rts))+' reconstructed timestep. No remove action requiered.')

def del_zts(zts_noex):
    if len(zts_noex)!=0:
        for zt in zts_noex:
            if is_number(zt):
                oftime = FoamTime(zt)
                oftime.rmZippedTime()
            else:
                pass
        print(str(len(zts))+' zipped timesteps deleted.')
    else:
        print(str(len(zts))+' zipped timestep. No remove action requiered.')
    
if len(sys.argv)>1:
    print('=======')
    print('Actions')
    print('=======')
    # actions according optional flags
    if args.tsRange!=None:
        rts = truncateSortedList(rts,args.tsRange[0],args.tsRange[1])
        zts_noex = truncateSortedList(zts_noex,args.tsRange[0],args.tsRange[1])
        rts_unmatch_zts = truncateSortedList(rts_unmatch_zts,args.tsRange[0],args.tsRange[1])
        zts_unmatch_rts = truncateSortedList(zts_unmatch_rts,args.tsRange[0],args.tsRange[1])
    else:
        pass        
    if args.dozip==True:
        dozip(rts_unmatch_zts)
    if args.dounzip==True:
        dounzip(zts_unmatch_rts)
    if args.del_rts==True:
        del_rts(rts)       
    if args.del_zts==True:
        del_zts(zts_noex)

    
    