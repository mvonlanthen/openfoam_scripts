#!/usr/bin/env python3

#======================================
# name:    sampleToBC.py
# author:  Marcel Vonlanthen
# date:    25.06.2013
# contact: marcelv@student.ethz.ch
# purpose: in OpenFOAM the tree structure generated by the sample tool differes
#          from the tree structure need by BC timeVaryingMappedFixedValue and its
#          derivated. sampleToBC.py reads a sample tree structure (input srcRoot)
#          and create the BC tree structure. It does not modified the files!
#          Notes:
#          * timeVaryingMappedFixedValue needs valid OF headers! Use BC
#            timeVaryingMappedFixedMappedValue to rule this limitation.
#          * Works only with foamFile output from sample tool.
#          * No advanced feature such as checking if file already exist in BC
#          
# version: 1.1
#======================================



#======================================
# import modules
#======================================
import os
import sys
import shutil
import argparse
import decimal as dcm


#======================================
# parse arguments
#======================================
parser = argparse.ArgumentParser(description='copy OpenFOAM sample planes into boundary condition data for BC timeVaryingMappedFixedValue (The file are not modified)')
parser.add_argument('srcRoot',
                    type=str,
                    help='folder which contains the sample planes generated by the OpenFOAM sample tool. It contains a list of time folder.'        
                    )
parser.add_argument('tgtRoot',
                    type=str,
                    help='folder which will contain the new boundary condition data.'        
                    )
parser.add_argument('-sn',
#                    '--samplename',
                    type=str,
                    dest='srcName',
                    required=True,
                    help='Name for source sample surface. Mendatory.'
                    )
parser.add_argument('-tn',
#                    '--targetname',
                    type=str,
                    dest='tgtName',
                    required=True,
                    help='name for target boudary condition. Mendatory'
                    )
parser.add_argument('-vl',
#                    '--variablelist',
                    type=str,
                    dest='varList',
                    nargs='+',
                    required=True,
                    help='list of variable to transfer. Mendatory'
                    )
parser.add_argument('-tsr',
#                    '--timesteprange',
                    type=float,
                    dest='tsRange',
                    nargs='+',
                    required=False,
                    help='range of timestep to transfer.'
                    )
parser.add_argument('-eb',
#                    '--ereasebc',
                    dest='eraseBC',
                    type=bool,
                    choices=[True,False],
                    default=False,
                    help='If the boundary already exist in srcRoot, erease it or not? Default=False'
                    )
parser.add_argument('-to',
                    type=float,
                    dest='timeOffset',
                    required=False,
                    default=0.0,
                    help='time offset'
                    )
parser.add_argument('-v',
#                    '--verbrose',
                    type=int,
                    dest='verbrose',
                    choices=[0, 1, 2],
                    default=0,
                    help='verbrosity level. Default = 0.'
                    )
parser.add_argument('-dp',
                    type=int,
                    dest='decimalPrecision',
                    required=True,
                    help='decimal precision: choose at the same order of magnitude than the source delta time'
                    )
args = parser.parse_args()

#print(args.srcRoot)
#print(args.tgtRoot)
#print(args.srcName)
#print(args.tgtName)
#print(args.varList)
#print(args.eraseBC)
#print(args.verbrose)
#======================================
# main
#======================================
def isNumber(s):
    '''Return True if 's' is a float'''
    try:
        float(s)
        return True
    except ValueError:
        return False
        
def findFile(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)
            print(root)
            print(dirs)

def getFoamTs(nb):
    '''
    converte a float or int (nb) into a valid OpenFOAM timestep (26 instead of 26.0) and return
    it as nbstr, a string. nbstr can be used in a path for example.
    '''
    nbstr = str(nb)
    if nbstr[-2:]=='.0':        # if nbstr='26.0'
        return nbstr[:-2]
    elif nbstr[-1]!='0':        #if nbstr='2.8' of '1.897' or '1.008': valid OF time
        return nbstr
    elif nbstr[-1]=='0' and len(nbstr)==1:   #if nbstr='0': valid OF time
        return nbstr
    else:                      #if nbrstr='26.000' or '3.4500'             
        trailingZero = True
        while trailingZero==True:
            nbstr = nbstr[:-1]
            if nbstr[-1]=='0':
                pass
            elif nbstr[-1]=='.':
                nbstr = nbstr[:-1]
                trailingZero = False
                return nbstr
            else:
                return nbstr
        

print('*************')
print('sampleToBC.py')
print('*************')
print('generate BC tree structure \"'+args.tgtRoot+'/'+args.tgtName+'\"'
      ' from sample tree structure \"'+args.srcRoot+'/timeStep/'+args.srcName+'\"'
      )
print('')

runDir = os.getcwd()
#os.chdir(args.srcRoot)
timeList = os.listdir(args.srcRoot)
for i in range(len(timeList)):
    if isNumber(timeList[i]):
        timeList[i] = float(timeList[i])
    else:
        timeList.pop(i)

timeList.sort()

if len(timeList)>0:
    pass
else:
    print('timeList is empty. Exit.')    
    sys.exit()

# get list of all surface available ("surfList") and if they are available for all
# timestep ("surfListBool")
#surfList = list()
#surfListBool = list()
##get list of surface for the first time, and add new surface if needed
#for plane in os.listdir(args.srcRoot+'/'+str(timeList[0])):
#    surfList.append(plane)
#for bol in [True for i in range(len(surfList))]:
#    surfListBool.append(bol)
## fill list of surface
#for timeStr in timeStrList:
#    surfs = os.listdir(args.srcRoot+'/'+timeStr)
#    for surf in surfs:
#        if not surf in surfList:
#            surfList.append(surf)
#            surfListBool.append(False)

    
# move post files to BC files
bcNamePath = args.tgtRoot+'/'+args.tgtName

#create path for BC
if not os.path.exists(bcNamePath):
    os.makedirs(bcNamePath)
elif args.eraseBC==True:
    shutil.rmtree(bcNamePath)
    os.makedirs(bcNamePath)
else:
    print('BC folder already exists and \'earaseBC\'=False. Exit')    
    sys.exit()

  
# create tree structure for BC
# -------------------------- #
#copy points file
pointsFile = 'points'
shutil.copy(args.srcRoot+'/'+getFoamTs(timeList[0])+'/'+args.srcName+'/'+pointsFile , bcNamePath+'/'+pointsFile)

#get variable type (scalarField, vectorField, etc...)
bcVarsRank = list()
for bcVar in args.varList:
    samplePath0 = args.srcRoot+'/'+getFoamTs(timeList[0])+'/'+args.srcName
    pathToVar = findFile(bcVar,samplePath0)
    bcVarsRank.append(pathToVar[len(samplePath0+'/'):-len('/'+bcVar)])

# copy variable from sample tree structure to BC tree structure
timeListRange = list()
if type(args.tsRange)==list:
    rangeIndex = [0,0]
    for i in range(len(timeList)):
        if timeList[i]==args.tsRange[0]:
            rangeIndex[0] = i
        if timeList[i]==args.tsRange[1]:
            rangeIndex[1] = i+1
    timeListRange = tuple(timeList[rangeIndex[0]:rangeIndex[1]])
else:
    timeListRange = timeList

dcm.getcontext().prec = args.decimalPrecision
for i in range(len(timeListRange)):
    srcTs = timeListRange[i]
    tgtTs = dcm.Decimal(timeListRange[i])+dcm.Decimal(args.timeOffset)
    if args.verbrose>=2:
        print('source time= '+str(srcTs))
        print('target time with decimal operation = '+str(tgtTs))
    tgtTsStr = getFoamTs(tgtTs)
    srcTsStr = getFoamTs(srcTs)
    if args.verbrose>=2:
        print('source time as valid OF time = '+srcTsStr)
        print('target time as valid OF time = '+tgtTsStr)
    os.makedirs(bcNamePath+'/'+tgtTsStr)
    if args.verbrose>=1:
        print('From source timestep '+getFoamTs(srcTs)+' to target timestep '+tgtTsStr+'. \"'+str(args.varList)+'\" copied')
    for i in range(len(args.varList)):
        varSrc = args.srcRoot+'/'+srcTsStr+'/'+args.srcName+'/'+bcVarsRank[i]+'/'+args.varList[i]
        varTgt = args.tgtRoot+'/'+args.tgtName+'/'+tgtTsStr+'/'+args.varList[i]
        shutil.copy(varSrc,varTgt)
        if args.verbrose>=2:
            print('copy \"'+varSrc+'\" to \"'+varTgt+'\"')




print('done')